package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstname;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastname;
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompanyname;
	@FindBy(how=How.NAME,using="submitButton") WebElement eleSubmit;
	//@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement eleLogin;
	
	public CreateLeadPage enterFirstname(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleFirstname, data);
		return this; 
	}
	public CreateLeadPage enterLastname(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(eleLastname, data);
		return this;
	}
	
	public CreateLeadPage enterCompanyname(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(eleCompanyname, data);
		return this;
	}
	
	public CreateLeadPage clickSubmit() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleSubmit);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return this;
	}
	
	
}













